import express from "express";
import axios from "axios";
import request from 'request';
import _ from 'underscore';

const router = express.Router();
// const fr24Url = 'https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=77.80,-84.00,-469.38,469.38&faa=1&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&stats=1';
//const fr24Url = 'https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=53.72,52.20,20.98,24.49&faa=1&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&stats=1';

const map = [60.42, 33.53, -26.36, 48.35];

// const fr24Url =
//   "https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=62.05,36.74,-21.72,34.35&faa=1&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&stats=1";
const fr24Url = `https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=${map[0]},${map[1]},${map[2]},${map[3]}&faa=1&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&stats=1&callsign=LO`;
const fr24DetailsUrl =
  "https://data-live.flightradar24.com/clickhandler/?version=1.5&flight=";

const mapSquares = [
  [30, 0, 0, 30],
  [30, 0, 30, 60],
  [30, 0, 60, 90],
  [30, 0, -30, 0],
  [30, 0, -60, -30],
  [30, 0, -90, -60],
];

const calculateSquares = (offsetX, offsetY) => {
  const zones = [];
  for (var x = -90; x <= 90 - offsetX; x += offsetX) {
    for (var y = -180; y <= 180 - offsetY; y += offsetY) {
      zones.push([x + offsetX, x, y, y + offsetY]);
    }
  }
  console.log(zones);
  return zones;
};

const getFrZoneData = (zoneArr) => {
  // console.log('getFroneData');
  // console.log(zoneArr);
  return new Promise((resolve, reject) => {
    // const zoneUrl = `https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=${zoneArr[0]},${zoneArr[1]},${zoneArr[2]},${zoneArr[3]}&faa=1&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&stats=1&callsign=LO`;
    // console.log(zoneUrl);
    // const fr24Response = axios.get(fr24Url)
    //   .then((response) => {
    //     console.log(_.keys(response.data).length);
    //     const frZoneData = _.omit(response.data, ['full_count', 'version', 'stats', 'selected-aircraft']);
    //     resolve(frZoneData);
    //   })
    //   .catch((err) => {
    //     reject(err);
    //   });

    request(`https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=${zoneArr[0]},${zoneArr[1]},${zoneArr[2]},${zoneArr[3]}&faa=1&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&stats=1&callsign=LO`, 
      { json: true }, (err, res, body) => {
        if (err) { reject(err); return; }

        // console.log(_.keys(body).length);
        const frZoneData = _.omit(body, ['full_count', 'version', 'stats', 'selected-aircraft']);
        resolve(frZoneData);
    });
  });
};

const getFrMapSquares = async (mapSquaresArr) => {
  return new Promise(async (resolve, reject) => {
    const frZonePromises = [];
    mapSquaresArr.forEach((mapSquare) => {
      frZonePromises.push(getFrZoneData(mapSquare));
    });
    const allZonesDataArr = await Promise.all(frZonePromises);
    const allZonesData = _.extend({}, ...allZonesDataArr);
    // console.log(allZonesData);
    console.log(`ALL ZONES COUNT: ${_.keys(allZonesData).length}`);
    resolve(allZonesData);
  });
};

router.get("/", async (req, res) => {

  const frZonesData = await getFrMapSquares(calculateSquares(45, 90));
  //const frZonesData = await getFrMapSquares([[54.65,51.63,19.26,25.90]]);
  //const frZonesData = await getFrZoneData([54.00,48.00,8.00,26.00]);
  //const squares = calculateZones(45);
  //console.log(squares);
  // res.send(frZonesData);
  // return;

  const fr24Info = frZonesData;
  // const count = fr24Info.full_count;
  // const version = fr24Info.version;

  const getFlightDetails = async key => {
    let fr24Details = await axios.get(`${fr24DetailsUrl}${key}`);
    fr24Details = fr24Details.data;
    const details = {};
    details.status = {
      text: fr24Details.status.text,
      color: fr24Details.status.icon
    };
    details.time = {
      scheduled: {
        arrival: fr24Details.time.scheduled.arrival,
        departure: fr24Details.time.scheduled.departure
      },
      real: {
        arrival: fr24Details.time.real.arrival,
        departure: fr24Details.time.real.departure
      }
    };
    return details;
  };

  const flightsInfo = [];
  const flightDetailsPromises = [];

  Object.keys(fr24Info).forEach(key => {
    if (key.toString().startsWith("2")) {
      const val = fr24Info[key];
      let flightInfo = {
        frId: key,
        flightNo: val[13],
        from: val[11],
        to: val[12],
        plane: val[8],
        planeNo: val[9],
        radar: val[7],
        lat: val[1],
        lon: val[2],
        speed: val[5],
        alt: val[4],
        track: val[3]
      };
      if (
        flightInfo.flightNo
          .toString()
          .toLowerCase()
          .startsWith("lo")
      ) {
        flightsInfo.push(flightInfo);
        flightDetailsPromises.push(getFlightDetails(flightInfo.frId));
      }
    }
  });

  const flightDetails = await Promise.all(flightDetailsPromises);
  flightsInfo.forEach((val, index) => {
    val.details = flightDetails[index];
  });

  // console.log(flightsInfo);
  res.send(flightsInfo);

  // const flightNumber = "LOT453";
  // const fr24Response = await axios.get(fr24Url);
  // const fr24Info = fr24Response.data;
  // const count = fr24Info.full_count;
  // const version = fr24Info.version;

  // const getFlightDetails = async key => {
  //   let fr24Details = await axios.get(`${fr24DetailsUrl}${key}`);
  //   fr24Details = fr24Details.data;
  //   const details = {};
  //   details.status = {
  //     text: fr24Details.status.text,
  //     color: fr24Details.status.icon
  //   };
  //   details.time = {
  //     scheduled: {
  //       arrival: fr24Details.time.scheduled.arrival,
  //       departure: fr24Details.time.scheduled.departure
  //     },
  //     real: {
  //       arrival: fr24Details.time.real.arrival,
  //       departure: fr24Details.time.real.departure
  //     }
  //   };
  //   return details;
  // };

  // const flightsInfo = [];
  // const flightDetailsPromises = [];

  // Object.keys(fr24Info).forEach(key => {
  //   if (key.toString().startsWith("2")) {
  //     const val = fr24Info[key];
  //     let flightInfo = {
  //       frId: key,
  //       flightNo: val[13],
  //       from: val[11],
  //       to: val[12],
  //       plane: val[8],
  //       planeNo: val[9],
  //       radar: val[7],
  //       lat: val[1],
  //       lon: val[2],
  //       speed: val[5],
  //       alt: val[4],
  //       track: val[3]
  //     };
  //     if (
  //       flightInfo.flightNo
  //         .toString()
  //         .toLowerCase()
  //         .startsWith("lo")
  //     ) {
  //       flightsInfo.push(flightInfo);
  //       flightDetailsPromises.push(getFlightDetails(flightInfo.frId));
  //     }
  //   }
  // });

  // const flightDetails = await Promise.all(flightDetailsPromises);
  // flightsInfo.forEach((val, index) => {
  //   val.details = flightDetails[index];
  // });

  //console.log(flightsInfo);
  //res.send(flightsInfo);
});

export default router;
