import express from "express";
import axios from "axios";
import request from 'request';
import _ from 'underscore';

const router = express.Router();

const calculateZones = (offsetX, offsetY) => {
  const zones = [];
  for (var y = -90; y <= 90 - offsetY; y += offsetY) {
    for (var x = -180; x <= 180 - offsetX; x += offsetX) {
      zones.push([y + offsetY, y, x, x + offsetX]);
    }
  }
  return zones;
};

const getFrZoneData = (zone, queryParams) => {
  return new Promise((resolve, reject) => {
    let zoneUrl = `https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=${zone[0]},${zone[1]},${zone[2]},${zone[3]}&faa=1&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&stats=1`;
    if (queryParams.callsign) {
      zoneUrl += `&callsign=${queryParams.callsign.toUpperCase()}`;
    }
    if (queryParams.from) {
      zoneUrl += `&from=${queryParams.from.toUpperCase()}`;
    }
    if (queryParams.to) {
      zoneUrl += `&to=${queryParams.to.toUpperCase()}`;
    }
    if (queryParams.type) {
      zoneUrl += `&type=${queryParams.type.toUpperCase()}`;
    }
    console.log(zoneUrl);
    request(zoneUrl,
      { json: true }, (err, res, body) => {
        if (err) {
          reject(err);
          return;
        }
        let zoneData = _.omit(body, ['full_count', 'version', 'stats', 'selected-aircraft']);
        // if (queryParams.callsign) {
        //   Object.keys(zoneData).forEach(key => {
        //     if (!zoneData[key][13].toString().toLowerCase().startsWith(queryParams.callsign)) {
        //       delete zoneData[key];
        //     }
        //   });
        // }
        resolve(zoneData);
      });
  });
};

const getFrZonesData = async (zonesArr, queryParams) => {
  return new Promise(async (resolve, reject) => {
    const frZonePromises = [];
    zonesArr.forEach((zone) => {
      frZonePromises.push(getFrZoneData(zone, queryParams));
    });
    const allZonesDataArr = await Promise.all(frZonePromises);
    const allZonesData = _.extend({}, ...allZonesDataArr);
    resolve(allZonesData);
  });
};

const getFlightsList = (zoneData) => {
  const flightsList = [];
  Object.keys(zoneData).forEach(key => {
    const val = zoneData[key];
    if (Array.isArray(val) && val.length >= 14) {
      const flight = {
        id: key,
        cs: val[13],
        from: val[11],
        to: val[12],
        plane: val[8],
        reg: val[9],
        radar: val[7],
        lat: val[1],
        lon: val[2],
        speed: val[5],
        alt: val[4],
        track: val[3]
      };
      flightsList.push(flight);
    }
  });
  //console.log(zoneData);
  return flightsList;
};

const getFlightDetails = async (flightId) => {
  return new Promise(async (resolve, reject) => {
    try {
      const details = await axios.get(`https://data-live.flightradar24.com/clickhandler/?version=1.5&flight=${flightId}`);
      //console.log(details);
      //console.log(`getFlightDetails ${details.data}`);
      resolve(details.data);
    }
    catch (err) {
      console.log(err);
      reject(err);
    }
  });
};

const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

const getFlightDetailsAll = async (flights) => {
  return new Promise(async (resolve, reject) => {
    // jesli ponad x rekordow, to rozbic na kilka transz (po 66 rekordow)
    const chunkLength = 40;
    const flightsChunk = _.chunk(_.pluck(flights, 'id'), chunkLength); // rozbija na tablice po 40 elementow
    //const flightsChunk = []; 
    //flightsChunk.push(_.pluck(flights, 'id'));
    const flightDetails = [];
    const flightDetailsPromises = [];

    //console.log(flightsChunk[0]);
    
    try {
      // flightsChunk.forEach((flightChunk, index) => {
      //   // const promisesChunk = flightChunk.map(async (flight) => flightDetails.push(getFlightDetails(flight.id)));
      //   const promisesChunk = [];
      //   flightChunk.forEach((flight) => {
      //     promisesChunk.push(getFlightDetails);
      //   });
      //   //console.log(`chunk length: ${promisesChunk.length}`);
      //   flightDetailsPromises.push(promisesChunk);
      // });

      //for (let i=0; i<flightsChunk.length; i++) {
        // flightsChunk[0].forEach(async (flight) => {
        //   flightDetails.push(await getFlightDetails(flight.id));
        // });
      //}

      //console.log(`chunks length: ${flightDetailsPromises.length}`);
      //console.log(`get first chunk length: ${flightDetailsPromises[0].length}`);
      //flightDetails.push(await Promise.all(flightDetailsPromises[0]));

      // for(let i=0; i<flightDetailsPromises.length; i++) {
      //   flightDetails.push(await Promise.all(flightDetailsPromises[i]));
      //   if (i<flightDetailsPromises.length-1) {
      //     await sleep(2000);
      //   }
      // }

      const getAll = async () => {
        for(let i=0; i<flightsChunk.length; i++) {
          console.log(`get ${i} chunk`);
          for(let j=0; j<flightsChunk[i].length; j++) {
            flightDetails.push(await getFlightDetails(flightsChunk[i][j]));
          }
          console.log(`sleep ${i}`);
          await sleep(1000);
        }
      }

      await getAll();

      //asyncForEach(flightDetailsPromises[0], () => { flightDetails.push() })

      // await asyncForEach(flightsChunk[0], getFlightDetails);

    }
    catch (err) {
      reject(err);
    }

    //console.log(flightDetails);

    //resolve(_.flatten(flightDetails));
    resolve(flightDetails);
  });
};

router.get("/", async (req, res) => {

  _.defaults(req.query, { callsign: 'lo' });

  console.log(req.query);

  const zonesData = await getFrZonesData(calculateZones(90, 90), req.query);
  //console.log(zonesData);
  const flights = getFlightsList(zonesData);
  // const flightDetailsPromises = [];
  // for(let i=0; i<66; i++) {
  //   flightDetailsPromises.push(getFlightDetails(flights[0].id));
  // }
  // flights.forEach((flight) => { flightDetailsPromises.push(getFlightDetails(flight.id)) });
  
  console.log(`flights count: ${flights.length}`);

  try {
    // parallel promise:
    // const flightDetails = await Promise.all(flightDetailsPromises);

    // sequence promise:
    //const flightDetails = [];
    // flightDetailsPromises.forEach(async (pr) => {
    //   flightDetails.push(await pr);
    // });
    // for(let i=0; i<flightDetailsPromises.length; i++) {
    //   flightDetails.push(await flightDetailsPromises[i]);
    // }

    const flightDetails = await getFlightDetailsAll(flights);

    flights.forEach((val, index) => {
      val.details = flightDetails[index];
      if (!val.details) {
        // defaultsy
        _.defaults(val, { details: { aircraft: {}, airport: {}, status: {}, time: { real: {}, scheduled: {}} } });
      }
    });
  }
  catch (err) {
    //console.log(err);
    res.status(500).send(`Cannot GET flight details. ${err.message}`);
    return;
  }

  res.send(flights);
});

export default router;