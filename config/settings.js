import path from 'path';
import appRoot from 'app-root-path';

const logsDir = () => {
  let dir = process.env.LOGS_DIR || path.join(appRoot.toString(), 'logs');
  dir = dir.replace('!APP_DIR!', appRoot.toString());
  return dir;
}

let settings = {
  nodeEnv: process.env.NODE_ENV || 'development',
  port: process.env.PORT || 3000,
  logsDir: logsDir()
};

module.exports = settings;
export default settings;