import path from 'path';
import { createLogger, format, transports } from 'winston';
import { logsDir } from './settings';

const { combine, colorize, timestamp, printf } = format;

const myFormat = printf(info => `${info.timestamp} ${info.level}: ${info.message}`);

const logger = createLogger({
  maxsize: 5242880, // 5MB
  maxFiles: 10,
  format: combine(
    timestamp(),
    myFormat
  ),
  transports: [
    new transports.File({ filename: path.join(logsDir, 'error.log'), level: 'error' }),
    new transports.File({ filename: path.join(logsDir, 'app.log') })
  ]
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(new transports.Console({
    format: combine(
      colorize(),
      myFormat
    )
  }));
}

// create a stream object with a 'write' function that will be used by `morgan`
logger.stream = {
  write(message) {
    // use the 'info' log level so the output will be picked up
    // by both transports (file and console)
    logger.info(message);
  },
};

module.exports = logger;
export default logger;