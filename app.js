#!/usr/bin/env node
/* eslint strict: 0 */

'use strict';

// enables ES6 ('import'.. etc) in Node
require('dotenv').config({ path: '.env' });
require('babel-core/register');
require('babel-polyfill');

const app = require('./main').default;
const http = require('http');
const settings = require('./config/settings');
const logger = require('./config/winston');

process.on('uncaughtException', (error) => {
  logger.error(error.toString());
});

process.on('unhandledRejection', (reason, p) => {
  logger.error(`Unhandled rejection at: ${p}. Reason: ${reason}`);
});

process.on('exit', (code) => {
  console.log('exit', code);
  logger.error(`App exit with code: ${code}`);
});

const normalizePort = (val) => {
  const port = parseInt(val, 10);
  if (isNaN(port)) {
    return val;
  }
  if (port >= 0) {
    return port;
  }
  return false;
};

const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

const server = http.createServer(app);

server.on('error', (error) => {
  if (error.syscall !== 'listen') {
    console.log('error', error);
    logger.error(error);
    throw error;
  }

  const bind = typeof port === 'string'
    ? `Pipe ${port}`
    : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      logger.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      logger.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
});

server.on('listening', () => {
  const addr = server.address();
  const bind = typeof addr === 'string'
    ? `pipe ${addr}`
    : `port ${addr.port}`;
  // debug(`Listening on ${bind}`);
  logger.info(`Listening on ${bind}`);
});

server.listen(port);