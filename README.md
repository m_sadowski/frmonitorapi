# FrMonitorApi

APi do pobierania listy lotów linii lotniczej LOT będących aktualnie w powietrzu. Dane pobierane z FlightRadar24.

Uruchomienie:
* yarn 
* yarn run start

Po wejściu pod adres:
http://localhost:3003/flights
zostaną zwrócone dane w postacji JSON-a.

Aplikacja działa pod adresem:
http://flights.msdev.ovh/flights