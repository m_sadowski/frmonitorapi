import 'dotenv/config'; // first of all import dotenv
import express from 'express';
import http from 'http';
import fs from 'fs';
import path from 'path';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import morgan from 'morgan';
import settings from './config/settings';
import logger from './config/winston';
import index from './routes/index';
import flights from './routes/flights';
import api from './routes/api';

let app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cors());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/flights', flights);
app.use('/api', api);
app.use('/', index);

// app.listen(settings.PORT);

// Create logs directory
const { logsDir } = settings;
if (!fs.existsSync(logsDir)) {
  fs.mkdirSync(logsDir);
}
logger.info(`Logs dir: ${logsDir}`);

export default app;